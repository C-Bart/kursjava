import sun.util.calendar.BaseCalendar;

import java.sql.Date;

public class Main {
    public static void main(String[] args) {

        Movie movie1 = new Thor();
//        System.out.println(movie1.getPlot());

        String text1 = "test";
        String text2 = "test1";

        System.out.println(movie1);
    }
}

class Movie {
    public Movie() {
        this.plot = "No plot";
    }

    public Movie(String plot) {
        this.plot = plot;
    }

    protected String plot;

    public String getPlot() {
        return plot;
    }
}

class Thor extends Movie {

    public Thor() {
        plot = "The big hammer";
    }

    public Thor(String plot) {
        super(plot);
    }
}

class BladeRunner extends Movie {
    public BladeRunner() {
        plot = "Police officer hunting android";
    }

    public BladeRunner(String plot) {
        super(plot);
    }
}

class It extends Movie {
    public It() {
        plot = "Blood, fear and violence... and clowns! And red baloons...";
    }

    public It(String plot) {
        super(plot);
    }
}

class ForrestGump extends Movie {
    public ForrestGump() {
        plot = "Run Forrest! RUN!";
    }

    public ForrestGump(String plot) {
        super(plot);
    }
}