// Klasa Kot dziedziąca po klasie Ssak
public class Kot extends Ssak {

    // zdefiniowana metoda tylko dla klasy podrzędnej
    public void kotMiauczy() {
        System.out.println("Miauuu... Aga dawaj żarcie... Miauuu...");
    }

    // nadpisanie metody zdefiniowanej w klasie nadrzędnej
    @Override
    public void ssakJe(double ileJe) {
        setCiezar(getCiezar() + ileJe);
        System.out.println(getNazwa()
                + " wpierdolił "
                + ileJe
                + " kg Whiskasa i teraz waży: "
                + getCiezar());
    }
}
