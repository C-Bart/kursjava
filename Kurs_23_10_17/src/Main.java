public class Main {

    public static void main(String[] args) {

        // Utworzenie dwóch referencji do dwóch obiektów typu Ssak za pomocą dwóch różnych konstruktorów
        Ssak s1 = new Ssak();
        Ssak s2 = new Ssak("Kot", "Kobieta");

        // Utworzenie referencji do okbiektu typu Kot
        Kot mruczuś = new Kot();

        // Przypisanie wartości do pola nazwa dla naszego obiektu - mruczuś
        mruczuś.setNazwa("Mruczuś");

        // Utworzenie referencji typu Ssak i przypisanie do niej obiektu utworzonego za pomocą klasy dziedziczącej Kot
        Ssak ssak = new Kot();

        // Wywołanie metody ssakJe na rzecz obiektu utworzonego wyżej, kompilator użyje właściwej metody, która jest nadpisana w klasie dziedziczącej
        ssak.ssakJe(100);

        // Wywołanie dwóch metod na rzecz obiektu typu Kot
        mruczuś.kotMiauczy();
        mruczuś.ssakNieZyje();

//        s2.ssakJe(100);
//        System.out.println(s2.getCiezar());

//        mruczuś.ssakNieZyje();
//        mruczuś.ssakJe(89);

    }
}