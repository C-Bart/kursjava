public class Ssak {

    // definicja pól klasy
    private String nazwa = "ssak";
    private int ilosc_nog = 4;
    private String plec = "gender";
    private double dlugosc_ciala = 45.6;
    private double ciezar = 150.6;

    // konstruktor domyślny
    public Ssak() {
    }

    // dodatkowy konstruktor
    public Ssak(String nazwa, String plec) {
        this.nazwa = nazwa;
        this.plec = plec;
    }

    public void ssakNieZyje() {
        System.out.println(getNazwa() + " nie żyje bo zdechł ginąc w straszliwych mękach pod kołami samochodu ciężarkowego prowadzonego przez pijanego ukraińskiego kierwce na ulicy Mogilskiej.");
    }

    public void ssakJe(double ileJe) {
        setCiezar(getCiezar() + ileJe);
        System.out.println(getNazwa()
                + " wpierdolił "
                + ileJe
                + " kg żarcia i teraz waży: "
                + getCiezar());
    }

    // Poniżej gettery i settery
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getIlosc_nog() {
        return ilosc_nog;
    }

    public void setIlosc_nog(int ilosc_nog) {
        this.ilosc_nog = ilosc_nog;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    public double getDlugosc_ciala() {
        return dlugosc_ciala;
    }

    public void setDlugosc_ciala(double dlugosc_ciala) {
        this.dlugosc_ciala = dlugosc_ciala;
    }

    public double getCiezar() {
        return ciezar;
    }

    public void setCiezar(double ciezar) {
        this.ciezar = ciezar;
    }
}
